# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lookme_cache/version'

Gem::Specification.new do |s|
  s.name = 'lookme-cache'
  s.version = LookmeCache::VERSION
  s.authors = ['chen.zhao']
  s.summary = 'lookme cache'

  s.add_runtime_dependency 'lookme-logging', '~> 0.0.1'

  s.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.require_paths = ['lib']
end
