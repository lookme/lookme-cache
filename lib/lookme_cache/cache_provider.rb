module LookmeCache
  class CacheProvider
    def fetch(key)
      raise ::NotImplementedError
    end

    def put(key, value, ttl)
      raise ::NotImplementedError
    end

    def delete(key)
      raise ::NotImplementedError
    end
  end
end