module LookmeCache
  class MarshalSerializer
    # @param [Object] object
    # @return [String]
    def to_wire(object)
      ::Marshal.dump(object)
    end

    # @param [String] object
    # @return [Object]
    def from_wire(object)
      ::Marshal.load(object)
    end
  end
end