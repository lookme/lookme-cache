module LookmeCache
  module CacheSupport
    attr_writer :cache_provider

    # @param [Object] key
    # @param [Integer] ttl
    def cache_fetch_or_put(key, ttl)
      value = @cache_provider.fetch(key)
      return value if value
      value = yield
      if value
        @cache_provider.put(key, value, ttl)
      end
      value
    end

    # @param [Object] key
    # @param [Integer] ttl
    # @param [Integer] compress_level
    def compress_cache_fetch_or_put(key, ttl, compress_level: Zlib::DEFAULT_COMPRESSION)
      value = @cache_provider.fetch(key)
      return ::Marshal.load(Zlib::Inflate.inflate(value)) if value
      value = yield
      if value
        @cache_provider.put(key, Zlib::Deflate.deflate(::Marshal.dump(value), compress_level), ttl)
      end
      value
    end
  end
end