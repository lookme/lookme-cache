module LookmeCache
  class NullCacheProvider < CacheProvider
    def fetch(key)
      nil
    end

    def put(key, value, ttl)
    end

    def delete(key)
    end
  end
end