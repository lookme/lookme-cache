module LookmeCache
  class RedisCacheProvider < CacheProvider
    LOG_TAG = self.name

    # @param [Redis] redis
    def initialize(redis)
      @logger = LookmeLogging::LoggerManager.find_logger(self.class)
      @redis = redis
      @serializer = MarshalSerializer.new
    end

    # @param [String] key
    def fetch(key)
      @logger.debug(LOG_TAG) {"fetch #{key}"}
      raw_value = @redis.get(key.to_s)
      return nil if raw_value.nil?
      @serializer.from_wire(raw_value)
    end

    # @param [String] key
    # @param [Object] value
    # @param [Integer] ttl
    def put(key, value, ttl)
      @logger.debug(LOG_TAG) {"put #{key}, ttl #{ttl}"}
      @redis.set(key.to_s, @serializer.to_wire(value), ex: ttl)
    end

    # @param [String] key
    def delete(key)
      @logger.debug(LOG_TAG) {"delete #{key}"}
      @redis.del(key.to_s)
    end
  end
end