# lookme-cache

## 紹介

キャッシュを使いやすいためのライブラリです。

## 使い方

### CacheProvider

いまはこれらのキャッシュプロバイダーがあります。

* NullCacheProvider
* RedisCacheProvider

NullCacheProviderというのはキャッシュしない意味です。

### RedisCacheProvider

RedisCacheProviderは名前通り、Redisを使ってキャッシュ機能を提供してます。
しかし、Redisは文字列あるいはバイト配列しか対応してないので、キャッシュの内容は変換する必要があります。

デフォルトではRubyのMarshalを利用して変換してます。
Marshalは基本的なタイプ、数値、文字列、ハッシュ、配列などに加え、クラスインスタンスを対応します。
でも、ActiveRecordのインスタンスは簡単なクラスインスタンスではないので、変換失敗の可能性があります。ご注意ください。
もしActiveRecordのインスタンスをキャッシュに入れたいなら、JSONに変換する方法があります。

### CacheSupport

もっと使いやすいように、LookmeCache::CacheSupportも用意しました。

使い方は

    class FooRepository
      include LookmeCache::CacheSupport

      def find_foo(id)
        cache_fetch_or_put("foo:#{id}", 5.minute) do
          do_find_foo(id)
        end
      end

      private

      def do_find_foo(id)
        # do stuff
      end
    end

    foo_repository = FooRepository.new
    foo_repository.cache_provider = LookmeCache::NullCacheProvider.new
    foo_repository.find_foo(1)

モジュールにあるメソッド `cache_fetch_or_put(key, ttl)` を使って、よくあるキャッシュとデータ取得のロジックを簡単に書けます。
そして、cache_providerという属性も宣言して、そとからセットできます。